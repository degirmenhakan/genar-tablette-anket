package housesociodemography.degirmen.genar.com.housesociodemography;

/**
 * Created by Enes on 25.02.2016.
 */
public class QueryBuilder {
    /**
     * Veritabanı ismini yazmamız gereken metod
     * @return
     */
    public String getDatabaseName() {
        return "genar";
    }

    /**
            * MongoLab API key bilgisini yazdım
    * @return
            */
    public String getApiKey() {
        return "rE1Vd2WnNMXvCRbq9o-ttmZPainGJppA";
    }

    /**
     * Veritabanındaki collections ve documents ile ilgili işlemler yapabilmemiz icn mongolab'da bulunan veritabanımıza
     * baglanmayı saglayan temel  URL hazırladım
     * @return
     */
    public String getBaseUrl()
    {
        return "https://api.mongolab.com/api/1/databases/"+getDatabaseName()+"/collections/";
    }
    /**
     * Hazırlanan BaseUrl'ye MongoLab API key ekledim
     * @return
     */
    public String docApiKeyUrl()
    {
        return "?apiKey="+getApiKey();
    }

    /**
     * collections ismini yazmamız gereken metod
     * @return
     */
    public String documentRequest()
    {
        return "Hane-Sosyo-Demografi";
    }

    /**
     * mongolab'da bulunan veritabanımıza baglanmayı saglayan URL tamamını hazırlayan metod
     * @return
     */
    public String buildContactsSaveURL()
    {
        return getBaseUrl()+documentRequest()+docApiKeyUrl();
    }

    /**
     * Android arayüzden alınan bilgilerin, veritabanımızda collections' a eklenecek documents formatlarını belirledim
     * @return
     */
    public String createContact(PollContract contact)
    {
        return String
                .format("{\"document\"  : {\"numberı\": \"%s\", "
                                + "\"name\": \"%s\", " +
                                "\"status\": \"%s\", "
                                + "\"gender\": \"%s\", " +
                                "\"age\": \"%s\", "
                                + "\"education\": \"%s\", " +
                                "\"maritalStatus\": \"%s\", "
                                + "\"jobStatus\": \"%s\"," +
                                " \"job\": \"%s\", "
                                +"\"retired\": \"%s\"}, \"safe\" : true}",
                        contact.getNumber(), contact.getName(), contact.getStatus(), contact.getGender(),contact.getAge(),contact.getEducation(),contact.getMaritalStatus(),contact.getJobStatus(),contact.getJob(),contact.getRetired());
    }

}
