package housesociodemography.degirmen.genar.com.housesociodemography;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Enes on 25.02.2016.
 */
public class SendDataService extends Service {

    Timer timer;
    Handler helper;

    final static long RATE = 6000;



    Button buton;
    ProgressDialog pDialog;
    String url = "http://192.168.0.14:1907/";
    String veri_string;
    PostClass post = new PostClass();  //Post Class dan post adında nesne olusturduk.Post classın içindeki methodu kullanabilmek için


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        timer= new Timer();
        helper= new Handler(Looper.getMainLooper());
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                sendData();
            }
        },0,RATE);


    }

    private void sendData(){


        final DB_SQLite db= new DB_SQLite(this);

        helper.post(new Runnable() {
            @Override
            public void run() {
                if (internetErisimi()) {
                    try {
                        if(getInternetSignalPower()>=40){
                            ArrayList<PollContract> datas = db.contractListUnUploaded();
                            for (int i = 0; i < datas.size(); ++i)
                            {
                                Log.d("Bilgi", "--> " + datas.get(i).toString());
                                db.updateViaID(datas.get(i).getId(), 1);
                                Post p1= new Post();
                                p1.setPollContract(datas.get(i));
                                p1.execute();
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });


    }
    public boolean internetErisimi() {

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService (Context.CONNECTIVITY_SERVICE);

        if (conMgr.getActiveNetworkInfo() != null

                && conMgr.getActiveNetworkInfo().isAvailable()

                && conMgr.getActiveNetworkInfo().isConnected()) {
            Log.d("Internet: ", conMgr.getActiveNetworkInfo().getState().name());

            //Toast.makeText(getBaseContext(),"Internet erisimi var " + conMgr.getActiveNetworkInfo().getState().name(),Toast.LENGTH_SHORT).show();
            return true;

        } else {

            return false;

        }

    }

    @Override
    public void onDestroy()
    {
        timer.cancel();
        super.onDestroy();
    }

/**
 *  Bağlı olunan wifinin sinyal gucunu donderen fonksiyon
 * @return WifiSignalPower
* @Referance=http://stackoverflow.com/questions/3437694/how-to-get-the-connection-strength-of-wifi-access-points
* */
    private int getInternetSignalPower() throws Exception {

        final WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        int state = wifi.getWifiState();
        int signalPower=0;
        if(state == WifiManager.WIFI_STATE_ENABLED) {
            List<ScanResult> results = wifi.getScanResults();

            for (ScanResult result : results) {
                if (result.BSSID.equals(wifi.getConnectionInfo().getBSSID())) {
                    int level = WifiManager.calculateSignalLevel(wifi.getConnectionInfo().getRssi(),
                            result.level);
                    int difference = level * 100 / result.level;
                    int signalStrangth = 0;
                    if (difference >= 100)
                        signalStrangth = 4;
                    else if (difference >= 75)
                        signalStrangth = 3;
                    else if (difference >= 50)
                        signalStrangth = 2;
                    else if (difference >= 25)
                        signalStrangth = 1;

                    Toast.makeText(getBaseContext(), "" + "Difference :" + difference + " signal state:" + signalStrangth+" ", Toast.LENGTH_SHORT).show();
                    //Log.d("Crypt", decryp);
                    signalPower=difference;
                }

            }
        }
        return signalPower;
    }



    class Post extends AsyncTask<Void, Void, Void> {

        PollContract p= new PollContract();
        protected void onPreExecute() { // Post tan önce yapılacak işlemler. Yükleniyor yazısını(ProgressDialog) gösterdik.
            pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Yükleniyor...");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false); // ProgressDialog u iptal edilemez hale getirdik.
            pDialog.show();
        }

        protected Void doInBackground(Void... unused) { // Arka Planda yapılacaklar. Yani Post işlemi

            List<NameValuePair> params = new ArrayList<NameValuePair>(); //Post edilecek değişkenleri ayarliyoruz.
            //Bu değişkenler bu uygulamada hiçbir işe yaramıyor.Sadece göstermek amaçlı
            params.add(new BasicNameValuePair("id",""+p.getId()));
            params.add(new BasicNameValuePair("number",""+p.getNumber()));
            params.add(new BasicNameValuePair("name", p.getName()));
            params.add(new BasicNameValuePair("status",""+p.getStatus()));
            params.add(new BasicNameValuePair("gender",""+p.getGender()));
            params.add(new BasicNameValuePair("age",""+p.getAge()));
            params.add(new BasicNameValuePair("education",""+p.getEducation()));
            params.add(new BasicNameValuePair("maritalStatus",""+p.getMaritalStatus()));
            params.add(new BasicNameValuePair("jobStatus",""+p.getJobStatus()));
            params.add(new BasicNameValuePair("job",""+p.getJob()));
            params.add(new BasicNameValuePair("retired",""+p.getRetired()));


            veri_string = post.httpPost(url,"GET",params,20000); //PostClass daki httpPost metodunu çağırdık.Gelen string değerini aldık

            Log.d("HTTP POST CEVAP:", "" + veri_string);// gelen veriyi log tuttuk

            return null;
        }

        protected void onPostExecute(Void unused) { //Posttan sonra
            pDialog.dismiss();  //ProgresDialog u kapatıyoruz.
           // Toast.makeText(getApplicationContext(),veri_string, Toast.LENGTH_LONG).show(); //Gelen veriyi Toast meaj ile 3 sn boyunca gösterdik
        }

        public void setPollContract(PollContract p1){
            p=p1;

        }
    }


}
