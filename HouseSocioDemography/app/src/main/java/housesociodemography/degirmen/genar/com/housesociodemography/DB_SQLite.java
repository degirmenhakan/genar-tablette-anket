package housesociodemography.degirmen.genar.com.housesociodemography;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Enes on 25.02.2016.
 */
public class DB_SQLite extends SQLiteOpenHelper {

    /*Database version*/
    private static  final  int DATABASE_VERSION=1;
    /*Database name*/
    private static final String DATABASE_NAME="Genar_Tablette_Anket";

    /*Table name*/
    private static final String TABLE_NAME="Hane";
    private static String ID="id";
    private static String NUMBER="number_";
    private static String NAME="name";
    private static String STATUS="status";
    private static String GENDER="gender";
    private static String AGE ="age";
    private static String EDUCATION="education";
    private static String MATERIALSTATUS="material_status";
    private static String JOBSTATUS="job_status";
    private static String JOB="job";
    private static String RETIRED="retired";
    private static String ISUPLOAD="isupdate";
    Context con;

    public DB_SQLite(Context context) {

        super(context,DATABASE_NAME,null,DATABASE_VERSION);
        con=context;

    }

    /*Databasei olusturan fonksiyon*/
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE="CREATE TABLE "+TABLE_NAME+" ("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,";
        CREATE_TABLE+=NUMBER+" INTEGER , "+ NAME+" TEXT , "+STATUS+" INTEGER , "+GENDER+" INTEGER , "+AGE+" INTEGER , ";
        CREATE_TABLE+= EDUCATION+" INTEGER , "+MATERIALSTATUS+" INTEGER ,  "+JOBSTATUS+" INTEGER , "+JOB+" INTEGER , ";
        CREATE_TABLE+=RETIRED+" INTEGER , "+ISUPLOAD+" INTEGER )";
        db.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertPollContract(PollContract contract){

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValue = new ContentValues();
            contentValue.put(NUMBER, contract.getNumber());
            contentValue.put(NAME, contract.getName());
            contentValue.put(STATUS, contract.getStatus());
            contentValue.put(GENDER, contract.getGender());
            contentValue.put(AGE, contract.getAge());
            contentValue.put(EDUCATION, contract.getEducation());
            contentValue.put(MATERIALSTATUS, contract.getMaritalStatus());
            contentValue.put(JOBSTATUS, contract.getJobStatus());
            contentValue.put(JOB, contract.getJob());
            contentValue.put(RETIRED, contract.getRetired());
            contentValue.put(ISUPLOAD, 0);
            db.insert(TABLE_NAME, null, contentValue);
            db.close();
        }catch (Exception ex){
            ShowErrorExceptionMessage(ex.getMessage());

        }
    }

    public  ArrayList<PollContract> contractList(){
        ArrayList<HashMap<String,String>> contracts= new ArrayList<HashMap<String, String>>();
        ArrayList<PollContract> pollContracts= new ArrayList<PollContract>();
        String selecetQuery="Select * FROM "+TABLE_NAME;
        SQLiteDatabase db= this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selecetQuery,null);

        cursor.moveToFirst();
        if(cursor.getCount()>0){
            do {

                HashMap<String, String> contract = new HashMap<>();
                PollContract pContract=new PollContract();
                pContract.setId(Integer.parseInt(cursor.getString(0)));
                pContract.setNumber(Integer.parseInt(cursor.getString(1)));
                pContract.setName(cursor.getString(2));
                pContract.setStatus(Integer.parseInt(cursor.getString(3)));
                pContract.setGender(Integer.parseInt(cursor.getString(4)));
                pContract.setAge(Integer.parseInt(cursor.getString(5)));
                pContract.setEducation(Integer.parseInt(cursor.getString(6)));
                pContract.setMaritalStatus(Integer.parseInt(cursor.getString(7)));
                pContract.setJobStatus(Integer.parseInt(cursor.getString(8)));
                pContract.setJob(Integer.parseInt(cursor.getString(9)));
                pContract.setRetired(Integer.parseInt(cursor.getString(10)));
                pContract.setIsUpload(Integer.parseInt(cursor.getString(11)));
                pollContracts.add(pContract);

                contract.put(ID, cursor.getString(0));
                contract.put(NUMBER, cursor.getString(1));
                contract.put(NAME, cursor.getString(2));
                contract.put(STATUS, cursor.getString(3));
                contract.put(GENDER, cursor.getString(4));
                contract.put(AGE, cursor.getString(5));
                contract.put(EDUCATION, cursor.getString(6));
                contract.put(MATERIALSTATUS, cursor.getString(7));
                contract.put(JOBSTATUS, cursor.getString(8));
                contract.put(JOB, cursor.getString(9));
                contract.put(RETIRED, cursor.getString(10));
                contract.put(ISUPLOAD, cursor.getString(11));
                Log.d("Info: ", "Size: " + pollContracts.size() + " ---> " + pContract.toString());
                contracts.add(contract);

            }while(cursor.moveToNext());

        }
        cursor.close();
        db.close();
        return pollContracts;


    }
    public ArrayList<PollContract> contractListUnUploaded(){
        ArrayList<HashMap<String,String>> contracts= new ArrayList<HashMap<String, String>>();
        ArrayList<PollContract> pollContracts= new ArrayList<PollContract>();
        // ArrayList<PollContract> contracts=new ArrayList<PollContract>();
        String selecetQuery="Select * FROM "+TABLE_NAME+ " Where "+ISUPLOAD+" = 0 ";
        SQLiteDatabase db= this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selecetQuery,null);

        cursor.moveToFirst();
        if(cursor.getCount()>0){
            do {

                HashMap<String, String> contract = new HashMap<>();
                PollContract pContract=new PollContract();
                pContract.setId(Integer.parseInt(cursor.getString(0)));
                pContract.setNumber(Integer.parseInt(cursor.getString(1)));
                pContract.setName(cursor.getString(2));
                pContract.setStatus(Integer.parseInt(cursor.getString(3)));
                pContract.setGender(Integer.parseInt(cursor.getString(4)));
                pContract.setAge(Integer.parseInt(cursor.getString(5)));
                pContract.setEducation(Integer.parseInt(cursor.getString(6)));
                pContract.setMaritalStatus(Integer.parseInt(cursor.getString(7)));
                pContract.setJobStatus(Integer.parseInt(cursor.getString(8)));
                pContract.setJob(Integer.parseInt(cursor.getString(9)));
                pContract.setRetired(Integer.parseInt(cursor.getString(10)));
                pContract.setIsUpload(Integer.parseInt(cursor.getString(11)));


                //contract.put(ID, cursor.getString(0));
                contract.put(NUMBER, cursor.getString(1));
                contract.put(NAME, cursor.getString(2));
                contract.put(STATUS, cursor.getString(3));
                contract.put(GENDER, cursor.getString(4));
                contract.put(AGE, cursor.getString(5));
                contract.put(EDUCATION, cursor.getString(6));
                contract.put(MATERIALSTATUS, cursor.getString(7));
                contract.put(JOBSTATUS, cursor.getString(8));
                contract.put(JOB, cursor.getString(9));
                contract.put(RETIRED, cursor.getString(10));
                contract.put(ISUPLOAD, cursor.getString(11));
                // Log.d("Info: ","Size: " + contract.size() + " ---> " + contract.toString());
                contracts.add(contract);
                pollContracts.add(pContract);

            }while(cursor.moveToNext());

        }
        cursor.close();
        db.close();
        return pollContracts;


    }
    public void updateViaID(int id,int value){
        SQLiteDatabase db = this.getWritableDatabase();
        //Bu methodda ise var olan veriyi güncelliyoruz(update)
        ContentValues values = new ContentValues();
        values.put(ISUPLOAD, value);


        // updating row
        db.update(TABLE_NAME, values, ID + " = ?",
                new String[] { String.valueOf(id) });

    }



    private void ShowErrorExceptionMessage(String ex){
        final AlertDialog errorAlert= new AlertDialog.Builder(con).create();
        errorAlert.setTitle("Hata");

            errorAlert.setMessage(ex);

        errorAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "Tamam", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                errorAlert.dismiss();

            }
        });
        errorAlert.show();

    }
}
