package housesociodemography.degirmen.genar.com.housesociodemography;

import android.content.Context;
import android.widget.Adapter;
import android.widget.ArrayAdapter;

/**
 * Created by Enes on 22.02.2016.
 */
public class PollContract {

    int number;
    String name="";
    int status=-1;
    int gender=-1;
    int age=0;
    int education=-1;
    int maritalStatus=-1;
    int jobStatus=-1;
    int job=-1;
    int retired=-1;
    int id;

    public int getIsUpload() {
        return isUpload;
    }

    public void setIsUpload(int isUpload) {
        this.isUpload = isUpload;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    int isUpload;

    PollContract(int number){
        this.number=number;
    }
    PollContract(){

    }
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getEducation() {
        return education;
    }

    public void setEducation(int education) {
        this.education = education;
    }

    public int getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(int maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public int getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(int jobStatus) {
        this.jobStatus = jobStatus;
    }

    public int getJob() {
        return job;
    }

    public void setJob(int job) {
        this.job = job;
    }

    public int getRetired() {
        return retired;
    }

    public void setRetired(int retired) {
        this.retired = retired;
    }

    @Override
    public String toString(){
        return id+" "+number+" "+name+" "+status+" "+gender+" "+age+" "+education+" "+maritalStatus+" "+jobStatus+" "+job+" "+retired+" "+isUpload;

    }
}
