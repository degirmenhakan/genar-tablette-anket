package housesociodemography.degirmen.genar.com.housesociodemography;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by degirmen on 20.02.2016.
 */
public class HouseSocioDemographyActivity extends Activity {

    int livingNumber=0;

    static List<PollContract> pollContracts;//= new ArrayList<PollContract>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        pollContracts=new ArrayList<PollContract>();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);
        final EditText et_livingPerson=((EditText)findViewById(R.id.hds_et_livingPerson));
        final EditText et_interviewPerson = (EditText) findViewById(R.id.hds_et_interview_person);
        final ListView lv_persons=(ListView)findViewById(R.id.hsd_listview_person);
        ImageButton im_btn_save=(ImageButton)findViewById(R.id.img_btn_save);

        /*Hane halkı sayısında bir değişiklik olduğunda yapılacak olan işlemler*/
        et_livingPerson.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(et_livingPerson.getText().length()==0){
                    et_interviewPerson.setEnabled(false);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               checkValidNumberOfPerson(s,et_livingPerson,et_interviewPerson);
                //lv_persons.setAdapter(null);

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()!=0) {
                    checkValidNumberOfPerson(s, et_livingPerson, et_interviewPerson);
                    //lv_persons.setAdapter(null);
                    createListView(lv_persons);
                }
            }
        });

        /*Interview yapilan kisi numarasında bir degisiklik yapildiginda*/
        et_interviewPerson.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkValidInterviewPersonNumber(et_interviewPerson);

            }
        });


        /*Liste Dolduruldugunda kaydetme islemi*/
        im_btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_livingPerson.getText().toString().trim().length()==0  ){

                    Toast.makeText(v.getContext(),"Hanede yaşayan kişi sayısı kısmı bos bırakılamaz",Toast.LENGTH_SHORT).show();
                }
                else if(et_interviewPerson.getText().toString().trim().length()==0){
                    Toast.makeText(v.getContext(),"Görüşülen kişi numarası boş bırakılamaz",Toast.LENGTH_SHORT).show();
                }else {

                    if(isFullName()){
                        //SaveAsyncTask tsk = new SaveAsyncTask();
                       DB_SQLite db= new DB_SQLite(getBaseContext());
                        for(int i=0;i<pollContracts.size();++i) {
                            Log.d("Uyarı", "" + pollContracts.get(i).toString());
                            db.insertPollContract(pollContracts.get(i));
                            Toast.makeText(getApplication(), "Bilgiler database kaydedildi.", Toast.LENGTH_SHORT).show();
                            //lv_persons.setEnabled(false);
                            }

                    }else{
                        Toast.makeText(v.getContext(),"Listede boş alan bırakılamaz...",Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

      SenDataServiceStart();


    }

    private boolean isFullName(){
         /*Listedeki tum isim alanları dolu mu kontrolu yapiliyor*/
        boolean isFullNameArea=true;
        for(int i=0;i<pollContracts.size();++i){
            if(pollContracts.get(i).getName().trim().length()==0){
                isFullNameArea=false;

            }

        }
        return isFullNameArea;

    }

    private void checkValidNumberOfPerson(CharSequence s, EditText et_livingPerson, EditText et_interviewPerson) {
        if (s.length() != 0) {/*Hanede yasayan kisi sayısı degistirildiğinde, bos olmadıgında*/
            livingNumber=Integer.parseInt(et_livingPerson.getText().toString());

            et_interviewPerson.setEnabled(true);

        } else {/*Hanede yasayan kisi sayısı degistirildiğinde fakat bos bırakıldıgında*/

            et_interviewPerson.setText("");
            et_interviewPerson.setEnabled(false);
        }

    }

    private void checkValidInterviewPersonNumber(EditText et_interviewPerson){
        /*Hanede yaşayan kisi sayisi dogru girildiginde */
        if (et_interviewPerson.getText().toString().length() != 0) {

            int numberOfInterviewPerson = Integer.parseInt(et_interviewPerson.getText().toString());
                    /*Gorüsülen kisi numarası hane halkı sayısından buyukse hata mesajı verip editTexti sifirliyoruz*/
            if (livingNumber < numberOfInterviewPerson ) {

                getErrorInterViewPerson(1);
                et_interviewPerson.setText("");
            }
            if(numberOfInterviewPerson==0){
                getErrorInterViewPerson(2);
                et_interviewPerson.setText("");
            }
        }

    }


    private void getErrorInterViewPerson(int section){
        final AlertDialog errorAlert= new AlertDialog.Builder(HouseSocioDemographyActivity.this).create();
        errorAlert.setTitle("Hata");
        if(section==1)
            errorAlert.setMessage("Görüşülen kişi numarası " + (livingNumber) + "'den büyük olamaz");
        else if(section==2)
            errorAlert.setMessage("Görüşülen kişi numarası 0 olamaz");

        errorAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "Tamam", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                errorAlert.dismiss();

            }
        });
        errorAlert.show();
    }


    private void createListView(ListView lv_persons){

         if(pollContracts.size()<=livingNumber)
         {
            for (int i = pollContracts.size(); i < livingNumber; ++i) {

                PollContract contract = new PollContract(i + 1);
                pollContracts.add(contract);

            }
       }else{
             for (int i = pollContracts.size(); i > livingNumber; --i) {
                 pollContracts.remove(pollContracts.size()-1);
             }
         }

        try {
            ListView yourListView = (ListView) findViewById(R.id.hsd_listview_person);
            PollAdapter customAdapter = new PollAdapter(this, pollContracts);
            yourListView.setAdapter(customAdapter);

            for(int i=0;i<pollContracts.size();++i)
                Log.d("Uyarı","Number:"+pollContracts.get(i).getNumber()+" Name"+pollContracts.get(i).getName()+" Status:"+pollContracts.get(i).getStatus());


        }catch (Exception e){
            getErrorExceptionMessage(e.getMessage());
        }
    }

    private void getErrorExceptionMessage(String message) {
        final AlertDialog errorAlert= new AlertDialog.Builder(HouseSocioDemographyActivity.this).create();
        errorAlert.setTitle("Hata");
        errorAlert.setMessage(message);
        errorAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "Tamam", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                errorAlert.dismiss();

            }
        });
        errorAlert.show();
    }


    /*Belli periyotlarda çalışacak olan servis fonksiyonları*/
    private boolean IsRunSenDataService()
    {
        ActivityManager serviceManagment = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        for(ActivityManager.RunningServiceInfo servis : serviceManagment.getRunningServices(Integer.MAX_VALUE))
        {
            if (getApplication().getPackageName().equals(servis.service.getPackageName()))
            {
                return true;
            }
        }
        return false;
    }

    public void SenDataServiceStart()
    {
      //  Button dugme = (Button) v;

        if(IsRunSenDataService())
        {
            stopService(new Intent(this, SendDataService.class));
        }
        else
        {
            startService(new Intent(this, SendDataService.class));

        }
    }




}
