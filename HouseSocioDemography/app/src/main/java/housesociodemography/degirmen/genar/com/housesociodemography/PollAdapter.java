package housesociodemography.degirmen.genar.com.housesociodemography;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Enes on 22.02.2016.
 */
public class PollAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<PollContract>  mKisiListesi;
    private  String[] valuesNumber;
    static Context context;


    int pos1=-1;

    public PollAdapter(Activity activity, List<PollContract> kisiler) {

        //XML'i alıp View'a çevirecek inflater'ı örnekleyelim
        mInflater = (LayoutInflater) activity.getSystemService( Context.LAYOUT_INFLATER_SERVICE);
        //gösterilecek listeyi de alalım
        mKisiListesi = kisiler;
        valuesNumber= new String[mKisiListesi.size()];
        for(int i=0;i<mKisiListesi.size();++i)
            valuesNumber[i]=""+mKisiListesi.get(i).getNumber();

    }

    @Override
    public int getCount() {
        return mKisiListesi.size();
    }

    @Override
    public Object getItem(int position) {
        return mKisiListesi.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if(convertView==null){

            holder = new ViewHolder();
            holder.p_contract= new PollContract();
            holder.ref=position;

            LayoutInflater inflater = mInflater;
            convertView = inflater.inflate(R.layout.info_column, parent, false);
            holder.txt_number = (TextView) convertView.findViewById(R.id.info_tv_number);

            /*Ad Soyad Kolonu*/
            holder.et_name=(EditText)convertView.findViewById(R.id.info_et_name);
            holder.et_name.setText(mKisiListesi.get(position).getName());
            holder.et_name.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    PollContract contract = (PollContract) holder.et_name.getTag();

                    holder.p_contract.setName(s.toString());
                    contract.setName(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            holder.et_name.setTag(mKisiListesi.get(position));



            /*Hanehalkı reisine göre yakınlıkları spinner ayarlanıyor*/
            holder.spin_status=(Spinner)convertView.findViewById(R.id.info_spinner_status);
            final ArrayAdapter spinStatusAdapter = ArrayAdapter.createFromResource(convertView.getContext(),R.array.status,android.R.layout.simple_spinner_item);
            spinStatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.spin_status.setAdapter(spinStatusAdapter);


            /*Cinsiyet spinner ayarlanıyor*/
            holder.spin_gender=(Spinner)convertView.findViewById(R.id.info_spinner_gender);
            final ArrayAdapter spinGenderAdapter=ArrayAdapter.createFromResource(convertView.getContext(), R.array.gender, android.R.layout.simple_spinner_item);
            spinGenderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
            holder.spin_gender.setAdapter(spinGenderAdapter);


            /*Egitim durumu spinner ayarlanıyor*/
            holder.spin_education=(Spinner)convertView.findViewById(R.id.info_spinner_education);
            ArrayAdapter spinnerEducationAdapter=ArrayAdapter.createFromResource(convertView.getContext(),R.array.education,android.R.layout.simple_spinner_item);
            spinnerEducationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);

            holder.spin_education.setAdapter(spinnerEducationAdapter);

            /*Medeni Durum spinner ayarlanıyor*/
            holder.spin_maritalStatus=(Spinner)convertView.findViewById(R.id.info_spinner_maritalStatus);
            ArrayAdapter spinnerMaritalAdapter=ArrayAdapter.createFromResource(convertView.getContext(),R.array.materialStatus,android.R.layout.simple_spinner_item);
            spinnerMaritalAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
            holder.spin_maritalStatus.setAdapter(spinnerMaritalAdapter);

            /*Calisma Durumu Spinner ayarlanıyor*/
            holder.spin_jobStatus=(Spinner)convertView.findViewById(R.id.info_spinner_jobStatus);
            ArrayAdapter spinnerJobStatusAdapter=ArrayAdapter.createFromResource(convertView.getContext(),R.array.jobStatus,android.R.layout.simple_spinner_item);
            spinnerJobStatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.spin_jobStatus.setAdapter(spinnerJobStatusAdapter);

            /*Meslek Spinner ayarlanıyor*/
            holder.spin_job=(Spinner)convertView.findViewById(R.id.info_spinner_job);
            ArrayAdapter spinJobAdapter=ArrayAdapter.createFromResource(convertView.getContext(),R.array.job,android.R.layout.simple_spinner_item);
            spinJobAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.spin_job.setPrompt("Meslek Seçiniz.");
            holder.spin_job.setAdapter(spinJobAdapter);

            /*Emekli Spinner ayarlanıyor*/
            holder.spin_retired=(Spinner)convertView.findViewById(R.id.info_spinner_retired);
            ArrayAdapter spinnerRetiredAdapter=ArrayAdapter.createFromResource(convertView.getContext(),R.array.retired,android.R.layout.simple_spinner_item);
            spinnerRetiredAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
            holder.spin_retired.setAdapter(spinnerRetiredAdapter);


            /*Yas */
            holder.et_age=(EditText)convertView.findViewById(R.id.info_editText_age);
            holder.et_age.setText("" + mKisiListesi.get(position).getAge());

            holder.et_age.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (holder.et_age.getText().equals("0"))
                        holder.et_age.setText("");
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (holder.et_age.getText().equals("0"))
                        holder.et_age.setText("");
                }

                @Override
                public void afterTextChanged(Editable s) {

                    try {

                        PollContract contract = (PollContract) holder.et_name.getTag();
                        int number = Integer.parseInt(s.toString());
                        if (number != 0)
                            holder.p_contract.setAge(number);
                        contract.setAge(number);
                        //mKisiListesi.get(position).setAge(number);
                        checkAge(contract.getAge(), holder.spin_education, holder.spin_maritalStatus, holder.spin_jobStatus, holder.spin_job, holder.spin_retired);

                    } catch (NumberFormatException ex) {


                    }


                }
            });



            convertView.setTag(holder);
            //context=convertView.getContext();

        }else {

            holder = (ViewHolder) convertView.getTag();
            ((ViewHolder) convertView.getTag()).et_name.setTag(mKisiListesi.get(position));
            holder.et_name.setText(mKisiListesi.get(position).getName());


        }
        holder.txt_number.setText(valuesNumber[position]);

        holder.spin_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (holder.ref == 0 && position != 0) {
                    ErrorPreNumber(parent.getContext());
                    holder.spin_status.setSelection(0);

                } else {
                    PollContract c = new PollContract(position);
                    c.setStatus(getStatusPosition(holder.spin_status.getSelectedItem().toString().trim()));
                    mKisiListesi.get(holder.ref).setStatus(c.getStatus());

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.spin_status.setSelection(mKisiListesi.get(position).getStatus());

        holder.spin_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                checkGender(position,holder.spin_job);
                mKisiListesi.get(holder.ref).setGender(holder.spin_gender.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.spin_gender.setSelection(mKisiListesi.get(position).getGender());



        holder.spin_education.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mKisiListesi.get(holder.ref).setEducation(getEducationPosition(holder.spin_education.getSelectedItem().toString().trim()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.spin_education.setSelection(mKisiListesi.get(position).getEducation());

        holder.spin_maritalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mKisiListesi.get(holder.ref).setMaritalStatus(holder.spin_maritalStatus.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.spin_maritalStatus.setSelection(mKisiListesi.get(position).getMaritalStatus());

        holder.spin_jobStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mKisiListesi.get(holder.ref).setJobStatus(holder.spin_jobStatus.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.spin_jobStatus.setSelection(mKisiListesi.get(position).getJobStatus());

        holder.spin_job.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mKisiListesi.get(holder.ref).setJob(getJobPosition(holder.spin_job.getSelectedItem().toString().trim()));
                checkRetired(position,holder.spin_retired,holder.et_name);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.spin_job.setSelection(mKisiListesi.get(position).getJob());

        holder.spin_retired.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mKisiListesi.get(holder.ref).setRetired(holder.spin_retired.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.spin_retired.setSelection(mKisiListesi.get(position).getRetired());

        convertView.setTag(holder);
        return convertView;
    }

    private void ErrorPreNumber(Context context){
        Toast.makeText(context,"İlk sırada hanehalkı reisi bulunmalıdır.",Toast.LENGTH_SHORT).show();

    }

    private void checkAge(int position,Spinner spinEducation,Spinner spin_maritalStatus,Spinner spin_jobStatus,Spinner spin_job,Spinner spin_retired){
        ArrayAdapter spinnerEducationAdapter;
        if(position<5){
            spinnerEducationAdapter=ArrayAdapter.createFromResource(spinEducation.getContext(),R.array.educationForChild,android.R.layout.simple_spinner_item);
            spinnerEducationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
            spinEducation.setAdapter(spinnerEducationAdapter);

        }else if(position<14){
            spinnerEducationAdapter=ArrayAdapter.createFromResource(spinEducation.getContext(),R.array.educationForChild3,android.R.layout.simple_spinner_item);
            spinnerEducationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
            spinEducation.setAdapter(spinnerEducationAdapter);

        }else if(position<18){

            spinnerEducationAdapter=ArrayAdapter.createFromResource(spinEducation.getContext(),R.array.educationforChild2,android.R.layout.simple_spinner_item);
            spinnerEducationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
            spinEducation.setAdapter(spinnerEducationAdapter);
        }else if(position<24){
            spinnerEducationAdapter=ArrayAdapter.createFromResource(spinEducation.getContext(),R.array.education2,android.R.layout.simple_spinner_item);
            spinnerEducationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
            spinEducation.setAdapter(spinnerEducationAdapter);

        }else{
            spinnerEducationAdapter=ArrayAdapter.createFromResource(spinEducation.getContext(),R.array.education,android.R.layout.simple_spinner_item);
            spinnerEducationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
            spinEducation.setAdapter(spinnerEducationAdapter);

        }

        if(position<15){

            spin_maritalStatus.setEnabled(false);
            spin_jobStatus.setEnabled(false);
            spin_job.setEnabled(false);
            spin_retired.setEnabled(false);


        }else{


            spin_maritalStatus.setEnabled(true);
            spin_jobStatus.setEnabled(true);
            spin_job.setEnabled(true);
            spin_retired.setEnabled(true);
        }

    }

    private void checkRetired(int position,Spinner spin_retired,EditText spin_age){


    }

    private void checkGender(int position,Spinner spinJob){
        ArrayAdapter spinJobAdapter;
        if(position==1){
            spinJobAdapter=ArrayAdapter.createFromResource(spinJob.getContext(),R.array.jobforMan,android.R.layout.simple_spinner_item);
            spinJob.setAdapter(spinJobAdapter);
        }else{
            spinJobAdapter=ArrayAdapter.createFromResource(spinJob.getContext(),R.array.job,android.R.layout.simple_spinner_item);
            spinJob.setAdapter(spinJobAdapter);

        }

     }



    private class ViewHolder {
        PollContract p_contract;
        TextView txt_number;
        EditText et_name;
        Spinner spin_status;
        Spinner spin_gender;
        EditText et_age;
        Spinner spin_education;
        Spinner spin_maritalStatus;
        Spinner spin_jobStatus;
        Spinner spin_job;
        Spinner spin_retired;

        int ref;
    }


    private int getStatusPosition(String st){

        if(st.equals("Hanehalkı Reisi"))
            return 0;
        else if(st.equals("Eşi"))
            return 1;
        else if(st.equals("Oğlu"))
            return 2;
        else if(st.equals("Kızı"))
            return 3;
        else if(st.equals("Babası"))
            return 4;
        else if(st.equals("Annesi"))
            return 5;
        else if(st.equals("Kardeşi"))
            return 6;
        else if(st.equals("Kayınpederi"))
            return 7;
        else if(st.equals("Kayınvalidesi"))
            return 8;
        else if(st.equals("Damadı"))
            return 9;
        else if(st.equals("Gelini"))
            return 10;
        else if(st.equals("Torunu"))
            return 11;
        else if(st.equals("Diğer Akrabası"))
            return 12;
        else if(st.equals("Evde Kalan Hizmetli"))
            return 13;
        else if(st.equals("Diğer"))
            return 14;
        else
            return -1;
    }
    private int getEducationPosition(String st){
        if(st.equals("Y.Lisans-Doktora vb."))
            return 0;
        else if(st.equals("Üniversite(Normal)"))
            return 1;
        else if(st.equals("Üniversite(Açıköğretim)"))
            return 2;
        else if(st.equals("YüksekOkul(2 Yıllık)"))
            return 3;
        else if(st.equals("Lise(normal)"))
            return 4;
        else if(st.equals("Lise(meslek)"))
            return 5;
        else if(st.equals("Ortaokul/ilköğretim(8 yıllık)"))
            return 6;
        else if(st.equals("İlkokul(5 yıllık)i"))
            return 7;
        else if(st.equals("Eğitimsiz(Okur-Yazar)"))
            return 8;
        else if(st.equals("Eğitimsiz(Okur-Yazar Değil)"))
            return 9;
        else if(st.equals("Okul öncesi(Okula Başlamamıs)"))
            return 10;
        else return -1;

    }
    private int getJobPosition(String st){
        if(st.equals("Emekli Çalışıyor."))
            return 0;
        else if(st.equals("Emekli Çalışmıyor. (Gelir getiren bir işi yok. Çalışmıyor)"))
            return 1;
        else if(st.equals("İşsiz şuan çalışmıyor-ek geliri yok, yardım alıyor"))
            return 2;
        else if(st.equals("İşsiz Şuan çalışmıyor-düzenli ek geliri var."))
            return 3;
        else if(st.equals("Ev kadını-ek geliri yok, yardım alıyor."))
            return 4;
        else if(st.equals("Ev kadını-düzenli ek geliri var."))
            return 5;
        else if(st.equals("Öğrenci (gelir getirici bir işi yok) ÜCRETLİ-MAAŞLI ÇALIŞIYOR"))
            return 6;
        else if(st.equals("İşçi / hizmetli-parça başı işi olan(düzensiz zaman zaman çalışan)"))
            return 7;
        else if(st.equals("İşçi / hizmetli-düzenli işi olan (özel bir sebep olmadıkça aynı işi yapan)"))
            return 8;
        else if(st.equals("Ustabaşı / kalfa-kendine bağlı işçi çalışan"))
            return 9;
        else if(st.equals("Yönetici olmayan memur / teknik eleman / uzman vs."))
            return 10;
        else if(st.equals("Yönetici (1-5 çalışanı olan)"))
            return 11;
        else if(st.equals("Yönetici (6-10 çalışanı olan)"))
            return 12;
        else if(st.equals("Yönetici (11-20 çalışanı olan)"))
            return 13;
        else if(st.equals("Yönetici (20'den fazla çalışanı olan)"))
            return 14;
        else if(st.equals("Ordu mensubu (uzman er, astsubay, subay)"))
            return 15;
        else if(st.equals("Ücretli Kıdemli Nitelikli uzman (avukat, doktor, mimar, mühendis, akademisyen vs)"))
            return 16;
        else if(st.equals("Çiftçi (kendi başına / ailesiyle çalışan)"))
            return 17;
        else if(st.equals("Seyyar-kendi işi (freelance dahil), dükkanda hizmet vermiyor"))
            return 18;
        else if(st.equals("Tek başına çalışan, dükkan sahibi, esnaf(ticari taksi sahibi)"))
            return 19;
        else if(st.equals("İşyeri sahibi 1-5 çalışanlı (Ticaret, Tarım, İmalat, Hizmet)"))
            return 20;
        else if(st.equals("İşyeri sahibi 6-10 çalışanlı (Ticaret, Tarım, İmalat, Hizmet)"))
            return 21;
        else if(st.equals("İşyeri sahibi 11-20 çalışanlı (Ticaret, Tarım, İmalat, Hizmet)"))
            return 22;
        else if(st.equals("İşyeri sahibi 20'den fazla çalışanlı (Ticaret, Tarım, İmalat, Hizmet)"))
            return 23;
        else if(st.equals("Serbest nitelikli uzman (Avukat, Mühendis, Mali müşavir, Doktor, Eczacı vs)"))
            return 24;
        else
            return -1;


    }

}


